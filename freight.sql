SELECT        Sales.Customer.AccountNumber, Person.Person.LastName, Person.Person.FirstName, Sales.Customer.CustomerID, Sales.Store.Name, Sales.SalesOrderHeader.SubTotal, Sales.SalesOrderHeader.TaxAmt, 
                         Sales.SalesOrderHeader.Freight, Sales.SalesOrderHeader.TotalDue,
                         (Sales.SalesOrderHeader.Freight/Sales.SalesOrderHeader.TotalDue)                         

FROM            Person.Person Left JOIN
                         Sales.Customer ON Person.Person.BusinessEntityID = Sales.Customer.PersonID Left JOIN
                         Sales.SalesOrderHeader ON Sales.Customer.CustomerID = Sales.SalesOrderHeader.CustomerID left JOIN
                         Sales.Store ON Sales.Customer.StoreID = Sales.Store.BusinessEntityID
ORDER BY Person.Person.LastName