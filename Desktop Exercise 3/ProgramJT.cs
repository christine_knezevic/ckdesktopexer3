﻿using Desktop_Exercise_3.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Data.SqlClient;
using System.Linq.Expressions;

namespace Desktop_Exercise_3
{
  class Program
  {
    static void Main(string[] args)
    {
      //inside this using statement, db is your database connection; the source of all of your queries
      //outside of the using statement, the database connection no longer exists and is cleaned up
      //by the garbage collector
      using (var db = new AdventureWorks2014Entities())
      {

        var customers = (from c in db.Customers
                         select c).ToList();

        //using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Code\ClassWork\CKDesktopExer3\Desktop Exercise 3\AccountNumber.txt")) ;
        using (StreamWriter file = new StreamWriter(@"C:\Code\ClassWork\CKDesktopExer3\Desktop Exercise 3\AccountNumber.txt"))
        {
          foreach (var cust in customers)
          {
            file.WriteLine(cust.AccountNumber);

            Console.WriteLine(cust.AccountNumber);
          }
        }

        //var accountNumber = from AccountNumber in db.Customers
        //                     select AccountNumber;
        //Customer.DataSource = db;

        //var accountNumber = db.Customers(accountNumber);

        //var accountNumber1 = from c in Customers where c.Customers(accountNumber1) select c;


        //select AccountNumber From Sales.Customer
      }

      //path to the output file contained in the project
      //var path = @"../../output.txt";




    }
  }
}
