﻿using Desktop_Exercise_3.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Data.SqlClient;
using System.Linq.Expressions;
using System.Xml;
using System.Data;
using MoreLinq;


namespace Desktop_Exercise_3
{
  class Program
  {
    static void Main(string[] args)
    {
      //inside this using statement, db is your database connection; the source of all of your queries
      //outside of the using statement, the database connection no longer exists and is cleaned up
      //by the garbage collector
      var myCustomers = new List<Customer>();
      using (var db = new AdventureWorks2014Entities())
      {
        //query #1
        //var customers = (from c in db.Customers
        //                 orderby c.AccountNumber
        //                 select c).ToList();

       var q1Path = @"../../Query1.txt";

        using (var swq1 = new StreamWriter(q1Path))
        {
          //query #1
          var customers = (from c in db.Customers
                           orderby c.AccountNumber
                           select c).ToList();

          var custnum = customers.Count();

          foreach (var cust in customers)
          {
            swq1.WriteLine(cust.AccountNumber);
            Console.WriteLine(cust.AccountNumber);
          }
          swq1.WriteLine(custnum);
          Console.WriteLine(custnum);
        }



        //myQuery2 = (from customers in db.Customers
        //            join people in db.People on customers.PersonID equals people.BusinessEntityID into pj
        //            from subPeople in pj.DefaultIfEmpty()
        //            join soh in db.SalesOrderHeaders on customers.CustomerID equals soh.CustomerID into sohj
        //            from subSoh in sohj.DefaultIfEmpty()
        //            join stores in db.Stores on customers.StoreID equals stores.BusinessEntityID into sj
        //            from subStores in sj.DefaultIfEmpty()
        //            orderby subPeople.LastName, subPeople.FirstName ascending
        //            select customers)
        //            .Include(x => x.Person)
        //            .Include(x => x.SalesOrderHeaders)
        //            .Include(x => x.Store)
        //            .ToList();



        //query #2
        var q2Path = @"../../Query2.txt";

        using (var swq2 = new StreamWriter(q2Path))
        {
          var custdata = (from c in db.Customers
                          join p in db.People on c.PersonID equals p.BusinessEntityID into poj
                          from pj in poj.DefaultIfEmpty()
                          join x in db.SalesOrderHeaders on c.CustomerID equals x.CustomerID into xoj
                          from xj in xoj.DefaultIfEmpty()
                          join s in db.Stores on c.StoreID equals s.BusinessEntityID into soj
                          from sj in soj.DefaultIfEmpty()
                          orderby pj.LastName, pj.FirstName ascending
                          select c)
                    .Include(x => x.Person)
                    .Include(x => x.SalesOrderHeaders)
                    .Include(x => x.Store)
                    .ToList();

          var custnum2 = 0;

          //var custdata = db.Customers
          //  .Include(x => x.Person)
          //  .Include(x => x.Store)
          //  .Include(x => x.SalesOrderHeaders)
          //  .OrderBy(x => x.Person.LastName)
          //  .ThenBy(x => x.Person.FirstName)
          //  .ThenBy(x => x.CustomerID)
          //  .ToList();

          foreach (var cust in custdata.Distinct())
          {
            custnum2++;

            swq2.WriteLine("**** Customer ID {0} ****", cust.CustomerID);
            swq2.WriteLine("Name = {0}, {1}; Store = {2}",
              (cust.Person == null) ? "NULL" : cust.Person.LastName,
              (cust.Person == null) ? "NULL" : cust.Person.FirstName,
              (cust.Store == null) ? "NULL" : cust.Store.Name);

            foreach (var h in cust.SalesOrderHeaders)
            {
              swq2.WriteLine("Subtotal = {0:C2}; Tax = {1:C2}; Freight = {2:C2}; Total = {3:C2}; Freight Percentage {4:0.00%}",
                              h.SubTotal,
                              h.TaxAmt,
                              h.Freight,
                              h.TotalDue,
                              (h.Freight / h.TotalDue));
            }

          }

          swq2.WriteLine(custnum2);
          Console.WriteLine(custnum2);
        }
        //query #3 looking for Chapman
        var q3Path = @"../../Query3.txt";

        using (var swq3 = new StreamWriter(q3Path))
        {
          var custdata = db.Customers
            .Include(x => x.Person)
            .Include(x => x.Store)
            .Include(x => x.SalesOrderHeaders)
            .Where(x => x.Person.LastName.Equals("Chapman"))
            .OrderBy(x => x.Person.LastName)
            .ThenBy(x => x.Person.FirstName)
            .ThenBy(x => x.CustomerID)
            .ToList();

          foreach (var cust in custdata)
          {
            swq3.WriteLine("**** Customer ID {0} ****", cust.CustomerID);
            swq3.WriteLine("Name = {0}, {1}; Store = {2}",
              (cust.Person == null) ? "NULL" : cust.Person.LastName,
              (cust.Person == null) ? "NULL" : cust.Person.FirstName,
              (cust.Store == null) ? "NULL" : cust.Store.Name);

            foreach (var h in cust.SalesOrderHeaders)
            {
              swq3.WriteLine("Subtotal = {0:C2}; Tax = {1:C2}; Freight = {2:C2}; Total = {3:C2}; Freight Percentage {4:0.00%}",
                              h.SubTotal,
                              h.TaxAmt,
                              h.Freight,
                              h.TotalDue,
                              (h.Freight / h.TotalDue));
            }

          }
          var custnum3 = custdata.Count();
          swq3.WriteLine(custnum3);
          Console.WriteLine(custnum3);
        }

        //query #4 total sales for each customer greater than $300,000
        var q4Path = @"../../Query4.txt";

        using (var swq4 = new StreamWriter(q4Path))
        {
          var custdata = db.Customers
            .Include(x => x.Person)
            .Include(x => x.Store)
            .Include(x => x.SalesOrderHeaders)
            .Where(x => x.SalesOrderHeaders.Sum(h => h.TotalDue) > (decimal)300000)
            .OrderBy(x => x.Person.LastName)
            .ThenBy(x => x.Person.FirstName)
            .ThenBy(x => x.CustomerID)
            .ToList();

          var custnum4 = 0;

          foreach (var cust in custdata)
          {
            custnum4++;

            swq4.WriteLine("**** Customer ID {0} ****", cust.CustomerID);
            swq4.WriteLine("Name = {0}, {1}; Store = {2}",
              (cust.Person == null) ? "NULL" : cust.Person.LastName,
              (cust.Person == null) ? "NULL" : cust.Person.FirstName,
              (cust.Store == null) ? "NULL" : cust.Store.Name);

            foreach (var h in cust.SalesOrderHeaders)
            {
              swq4.WriteLine("Subtotal = {0:C2}; Tax = {1:C2}; Freight = {2:C2}; Total = {3:C2}; Freight Percentage {4:0.00%}",
                              h.SubTotal,
                              h.TaxAmt,
                              h.Freight,
                              h.TotalDue,
                              (h.Freight / h.TotalDue));
            }
            swq4.WriteLine("Sales Total = {0:C2}", (cust.SalesOrderHeaders.Sum(h => h.TotalDue)));
          }
          
          swq4.WriteLine(custnum4);
          Console.WriteLine(custnum4);
        }

        //query #6 highest amount of freight paid by one customer
        var q6Path = @"../../Query6.txt";

        using (var swq6 = new StreamWriter(q6Path))
        {
          var bigFreight = db.SalesOrderHeaders.OrderByDescending(x => x.Freight / x.TotalDue).First();

          var custdata = db.Customers
            .Include(x => x.Person)
            .Include(x => x.Store)
            .Include(x => x.SalesOrderHeaders)
            .OrderBy(x => x.Person.LastName)
            .Where(x => x.CustomerID == bigFreight.CustomerID)
            .ToList();

          var custnum6 = custdata.Count();

          //var largeFreightPercent = db.SalesOrderHeaders.MaxBy(x => x.Freight / x.TotalDue);

          foreach (var cust in custdata)
          {
            swq6.WriteLine("**** Customer ID {0} ****", cust.CustomerID);
            swq6.WriteLine("Name = {0}, {1}; Store = {2}",
              cust.Person.LastName,
              cust.Person.FirstName,
              cust.Store.Name);

            foreach (var h in cust.SalesOrderHeaders)
            {
              swq6.WriteLine("Subtotal = {0:C2}; Tax = {1:C2}; Freight = {2:C2}; Total = {3:C2}; Freight Percentage {4:0.00%}",
                    h.SubTotal, h.TaxAmt, h.TotalDue,
                    h.Freight, (h.Freight / h.TotalDue));
            }
            swq6.WriteLine("Sales Total = {0:C2}", (cust.SalesOrderHeaders.Sum(h => h.SubTotal)));
          }
          var theCust = custdata.First();

          swq6.WriteLine("**** Max Freight Found ****");
          swq6.WriteLine("**** Customer ID {0} ****", theCust.CustomerID);
          swq6.WriteLine("Name = {0}, {1}; Store = {2}",
            theCust.Person.LastName,
            theCust.Person.FirstName,
            theCust.Store.Name);

          //foreach (var h in theCust.SalesOrderHeaders)
          //{ 
          swq6.WriteLine("Subtotal = {0:C2}; Tax = {1:C2}; Freight = {2:C2}; Total = {3:C2}; Freight Percentage {4:0.00%}",
            bigFreight.SubTotal, bigFreight.TaxAmt, bigFreight.TotalDue,
            bigFreight.Freight, (bigFreight.Freight / bigFreight.TotalDue));
          //}
          swq6.WriteLine(custnum6);
          Console.WriteLine(custnum6);
        }
        //swq6.WriteLine("**** Customer ID {0} ****", largeFreightPercent.CustomerID);
        //swq6.WriteLine("Name = {0}, {1}; Store = {2}",
        //  largeFreightPercent.Customer.Person.LastName,
        //  largeFreightPercent.Customer.Person.FirstName,
        //  largeFreightPercent.Customer.Store.Name);

        //swq6.WriteLine("Subtotal = {0:C2}; Tax = {1:C2}; Freight = {2:C2}; Total = {3:C2}; Freight Percentage {4:0.00%}",
        //      largeFreightPercent.SubTotal, largeFreightPercent.TaxAmt, largeFreightPercent.TotalDue,
        //      largeFreightPercent.Freight, (largeFreightPercent.Freight / largeFreightPercent.TotalDue));

        //swq6.WriteLine("Sales Total = {0:C2}", (largeFreightPercent.Customer.SalesOrderHeaders.Sum(h => h.SubTotal)));

        //swq6.WriteLine("**** Customer ID {0} ****", l
        //  .CustomerID);
        //  swq6.WriteLine("Name = {0}, {1}; Store = {2}",
        //    (cust.Person == null) ? "NULL" : cust.Person.LastName,
        //    (cust.Person == null) ? "NULL" : cust.Person.FirstName,
        //    (cust.Store == null) ? "NULL" : cust.Store.Name);

        // foreach (var h in cust.SalesOrderHeaders)
        //  {
        //swq6.WriteLine("Subtotal = {0:C2}; Tax = {1:C2}; Freight = {2:C2}; Total = {3:C2}; Freight Percentage {4:0.00%}",
        //                h.SubTotal,
        //                h.TaxAmt,
        //                h.Freight,
        //                h.TotalDue,
        //                (h.Freight / h.TotalDue));
        //    }
        //swq6.WriteLine("Sales Total = {0:C2}", (cust.SalesOrderHeaders.Sum(h => h.SubTotal)));
        //  }
        //var custnum6 = largeFreightPercent.Count();
        //swq6.WriteLine(custnum6);
        //Console.WriteLine(custnum6);
        //  }
      }
    }
  }
}


